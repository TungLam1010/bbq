import APP from './Constant.json';
export default class Ball extends PIXI.Sprite{
  constructor(levelContainer, dx, dy, x,y, texture) {
    super(texture);
    this.levelContainer = levelContainer;
    this.dx = dx;
    this.dy = dy;
    this.x = x;
    this.y = y;
  }
  getCollision(x){
    if (x == 'left' || x == 'right') {
      this.dx = -this.dx;
    }
    if (x == 'top' || x == 'bottom') {
      this.dy = -this.dy;
    }
  }

  updatePosition(){
    this.x += this.dx;
    this.y += this.dy;

    if(this.x + this.dx +26 > APP.APP.width ) {
      this.dx = -this.dx;
      this.graphics2 = new PIXI.Graphics();
      this.graphics2.lineStyle(12, 0xEEE1EE, 1);
      this.graphics2.moveTo(APP.APP.width, 0);
      this.graphics2.lineTo(APP.APP.width, APP.APP.height);
      this.graphics2.closePath();
      this.graphics2.endFill();
      this.levelContainer.addChild(this.graphics2);
    } else {
      this.levelContainer.removeChild(this.graphics2);
    }
    if( this.x + this.dx < 0 ) {
      this.dx = -this.dx;
      this.graphics3 = new PIXI.Graphics();
      this.graphics3.lineStyle(12, 0xEEE1EE, 1);
      this.graphics3.moveTo(0, 0);
      this.graphics3.lineTo(0, APP.APP.height);
      this.graphics3.closePath();
      this.graphics3.endFill();
      this.levelContainer.addChild(this.graphics3);
    } else {
      this.levelContainer.removeChild(this.graphics3);
    }
    if (this.y + this.dy + 26 > APP.APP.height) {
      this.dx = this.dy = 0;
      this.graphics1 = new PIXI.Graphics();
      this.graphics1.lineStyle(12, 0xEEE1EE, 1);
      this.graphics1.moveTo(0, APP.APP.height);
      this.graphics1.lineTo(APP.APP.width, APP.APP.height);
      this.graphics1.closePath();
      this.graphics1.endFill();
      this.levelContainer.addChild(this.graphics1);
    } else {
      this.levelContainer.removeChild(this.graphics1);
    }
    if(this.y + this.dy < APP.APP.height*23.1481481481/100 ) {
      this.dy = -this.dy;
      this.graphics = new PIXI.Graphics();
      this.graphics.lineStyle(12, 0xEEE1EE, 1);
      this.graphics.moveTo(0, APP.APP.height*23.1481481481/100);
      this.graphics.lineTo(APP.APP.width, APP.APP.height*23.1481481481/100);
      this.graphics.closePath();
      this.graphics.endFill();
      this.levelContainer.addChild(this.graphics);
    } else {
      this.levelContainer.removeChild(this.graphics);
    }
  }
  
}
