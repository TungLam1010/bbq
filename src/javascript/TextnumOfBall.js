import APP from './Constant.json';

export default class TextNumOfBall extends PIXI.Container {
  constructor(levelContainer, dataBall){
    super();
    this.levelContainer = levelContainer;
    this.num = dataBall.numOfBalls;
    this.PositionBall = APP.APP.width/2 + APP.APP.width*2.77777777778/100;
  }
  getPositionOfBall(x){
    this.children[0].x = x+ APP.APP.width*2.77777777778/100;
  }
  numOfBall(){
    let text = new PIXI.Text('',{fontFamily : 'Arial', fontSize: 24, fill : 0xffffff, align : 'center'});
    text.x = this.PositionBall;
    text.y = APP.APP.height - APP.APP.height*4.62962962963/100;
    text.text = 'x'+this.num;
    this.addChild(text);
    this.levelContainer.addChild(this);
  }
}