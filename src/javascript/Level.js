import BallContainer from "./BallContainer";
import BrickContainer from "./BrickContainer";
import CollisionDetection from "./CollisionDetedtion";
import APP from "./Constant.json";
import FirstBall from "./FirstBall";
import TextNumOfBall from "./TextNumOfBall";
import Score from "./Score";

export default class Level extends PIXI.Container {
  constructor(scenceContainer, dataOflevel, app) {
    super();
    this.app = app;
    this.scenceContainer = scenceContainer;
    this.dataOflevel = dataOflevel;
    this.XofBall = APP.APP.width / 2;
    this.YofBall = APP.APP.height - (APP.APP.height * 2.77777777778) / 100;
    this.checkCollision = true;
    this.pointX = 0;
    this.setupLevel();
    window.addEventListener("dblclick", () => {
      this.stopBalls();
    });
  }
  setupLevel() {
    this.brickContainer = new BrickContainer(
      this,
      this.dataOflevel.BRICKS,
      this.app
    );
    this.brickContainer.addBricks();
    this.ballContainer = new BallContainer(
      this.XofBall,
      this.YofBall,
      this.dataOflevel.BALL,
      this,
      this.app
    );
    this.addChild(this.ballContainer);
    let textureBall = PIXI.Texture.from("../images/BAL041.png");
    this.firstBall = new FirstBall(textureBall, this);
    this.firstBall.position.set(this.XofBall, this.YofBall);
    this.TextNumOfBall = new TextNumOfBall(this, this.dataOflevel.BALL);
    this.TextNumOfBall.numOfBall();
    this.score = new Score(
      this,
      this.dataOflevel.BRICKS,
      this.scenceContainer,
      this.dataOflevel.BALL,
      this.app
    );
    this.score.addScore();

    this.scenceContainer.addChild(this);
  }
  stopBalls() {
    if (this.ballContainer.children[0] !== undefined) {
      this.checkCollision = false;
      this.ballContainer.children.forEach((ball) => {
        ball.dx = 0;
        ball.dy = 0;
        setTimeout(() => {
          new TWEEN.Tween(ball).to(
            { x: this.XofBall, y: this.YofBall },
            400
          ).start();
        }, 100);
      });
    }
  }
  update(dt) {
    this.ballContainer.children.forEach((ball) => {
      ball.updatePosition();
      if (this.ballContainer.children[0] !== undefined) {
        if (this.brickContainer.children[1] !== 0) {
          this.brickContainer.children.forEach((brick) => {
            this.CollisionDetection = new CollisionDetection(
              ball.dx,
              ball.dy,
              ball.x,
              ball.y,
              ball.width / 2,
              brick.x-25,
              brick.y-25,
              50,
              50,
              brick
            );
            if (this.checkCollision) {
              ball.getCollision(this.CollisionDetection.Coliision().collision);
              if (this.CollisionDetection.Coliision().collision !== undefined) {
                this.brickContainer.getBricksCollision(brick);
              }
              // this.brickContainer.getBricksCollision(
              //   this.CollisionDetection.Coliision().brick
              // );
              this.score.getScore(
                this.CollisionDetection.Coliision().brick,
                brick.y
              );
              // this.ballContainer.checklevelUp(this.CollisionDetection.Coliision().brick, this.dataOflevel.BALL);
            }
          });
        }
      }
      if (ball.dx == 0 && ball.dy == 0) {
        this.TextNumOfBall.visible = true;
        this.count++;
      }
      if (ball.dx !== 0 && ball.dy !== 0) {
        this.TextNumOfBall.visible = false;
        this.firstBall.removeBall(true);
        this.count = 0;
      }
      this.ballContainer.getPosition(
        ball.x,
        ball.y,
        ball.dx,
        ball.dy,
        this.count
      );
      this.brickContainer.changePositionBricks(
        this.count,
        this.dataOflevel.BALL
      );
    });
    if (this.ballContainer.children[0] !== undefined) {
      for (let i = 0; i < this.ballContainer.children.length; i++) {
        if (
          [this.ballContainer.children[i].dx].some((dx) => dx == 0) &&
          this.pointX == 0
        ) {
          this.pointX = this.ballContainer.children[i].x;
          this.TextNumOfBall.getPositionOfBall(this.pointX);
        }
        if ([this.ballContainer.children[i].dx].every((dx) => dx !== 0)) {
          this.pointX = 0;
        }
        if (this.pointX !== 0) {
          break;
        }
      }
    }
    if (this.ballContainer.children[0] !== undefined) {
      this.ballContainer.children.forEach((ball) => {
        if (
          ball.dx == 0 &&
          ball.dy == 0 &&
          ball.y + ball.dy > APP.APP.height - 50
        ) {
          this.tween = new TWEEN.Tween(ball).to(
            { x: this.pointX, y: this.YofBall },
            1
          );
          this.tween.start();
          this.checkCollision = true;
        }
      });
    }
    this.brickContainer.update(dt);
  }
}
