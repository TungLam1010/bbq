import APP from './Constant.json';
export default class Border {
  constructor(app) {
    this.app = app;
    const graphics = new PIXI.Graphics();
    graphics.lineStyle(10, 0xc1558d, 1);

    graphics.moveTo(0, 300 );
    graphics.lineTo(APP.APP.width, 300 );
    graphics.closePath();
    graphics.endFill();

    graphics.moveTo(0, 0);
    graphics.lineTo(0, APP.APP.height);
    graphics.closePath();
    graphics.endFill();

    graphics.moveTo(APP.APP.width, 0);
    graphics.lineTo(APP.APP.width, APP.APP.height);
    graphics.closePath();
    graphics.endFill();

    graphics.moveTo(0, APP.APP.height);
    graphics.lineTo(APP.APP.width, APP.APP.height);
    graphics.closePath();
    graphics.endFill();

    this.app.stage.addChild(graphics);
  }
}
