export default class DataImages {
  constructor() {}
  getPixelDataFromImage(x, y) {
    return {
      r: y * 14 * 4 + x *4,
      g: y * 14 * 4 + x * 4 + 1,
      b: y * 14 * 4 + x * 4 + 2,
      a: y * 14 * 4 + x * 4 + 3,
    };
  }
  getdataFromPixelImage(x, num) {
    let tempX = (x / 4) % 14;
    let tempY = Math.trunc((x/4) / 14);
    
    return { x: tempX, y: tempY, num: num };
  }
}
