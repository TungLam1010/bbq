
export default class FirstBall extends PIXI.Sprite {
  constructor(texture, levelContainer){
    super(texture);
    this.levelContainer = levelContainer;
    this.levelContainer.addChild(this);
  }
  removeBall(checkShotBall){
    if (checkShotBall) {
      this.levelContainer.removeChild(this);
    }
  }
}