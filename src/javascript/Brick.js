export default class Brick extends PIXI.Sprite {
  constructor(app) {
    super(app.loader.resources.data.textures.blocks);
  }
}