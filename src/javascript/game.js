import Scene from "./Scene";

export default class Game {
  constructor(app) {
    this.app = app;
  }
  setup() {
    this.scene = new Scene(this.app);
    this.app.ticker.add((delta) => {
      this.update(delta), this;
    });
  }
  update(dt) { 
    this.scene.update(dt);
    TWEEN.update();
  }
}
