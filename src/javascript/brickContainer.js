import APP from './Constant.json';
import ParticlesContainer from './ParticlesContainer';
import Warning from './Warning';
import * as PIXI from 'pixi.js';

export default class BrickContainer extends PIXI.Container {
  constructor(containerLevel, dataBrick, app) {
    super();
    this.app = app;
    this.containerLevel = containerLevel;
    this.particles = new ParticlesContainer(this.containerLevel, 200);
    this.dataBrick = dataBrick;
    this.warning = new Warning(this.containerLevel, this.app);
    this.warning.visible = false;
    new TWEEN.Tween(this.warning)
      .to({ alpha: 0 }, 1000)
      .yoyo(true)
      .repeat(Infinity)
      .start();
  }
  
  update(dt) {
    this.particles.update(dt);
  }
  changePositionBricks(count, numofBall) {
    let checkGameOver = false;
    if (count == numofBall.numOfBalls) {
      this.children.forEach((brick) => {
        brick.y += 50;
        if (brick.y > 1150) {
          checkGameOver = true;
          return;
        }
      });
    }
    this.children.forEach((brick) => {
      if ([brick.y].some((XofBrick) => XofBrick > 1100)) {
        this.warning.visible = true;
      }
      if ([brick.y].every((YofBrick) => YofBrick < 1100)) {
        this.warning.visible = false;
      }
    });
    if (checkGameOver) {
      setTimeout(() => {
        while (this.children[0]) {
          this.removeChild(this.children[0]);
        }
        this.addBricks();
      }, 1000);
    }
  }
  getBricksCollision(brick) {
    if (brick !== undefined) {
      // brick.pivot.set(0.5, 0.5);
      
      new TWEEN.Tween(brick)
        .to( {scale: {x: 1.75, y: 1.75}, alpha: 2}, 10)
        .yoyo(true)
        .repeat(1).start();
      brick.value--;
      brick.children[0].text = brick.value;
      if (brick.value == 0 ) {
        this.removeBrick = this.particles.getChild().data;
        this.removeBrick.spawnCircle.x = brick.x;
        this.removeBrick.spawnCircle.y = brick.y;
        this.removeChild(brick);
      }
    }
  }
  addBricks() {
    for (let i = 0; i < this.dataBrick.length; i++) {
      let brick = new  PIXI.Sprite(this.app.loader.resources.brick.texture);
      brick.value = this.dataBrick[i].num;
      let scaleFactor = 50/brick.width;
      brick.scale = new PIXI.ObservablePoint(null, brick, scaleFactor, scaleFactor);
      brick.x = APP.APP.paddingLeft + this.dataBrick[i].x * brick.width;
      brick.y = APP.APP.paddingtop + this.dataBrick[i].y * brick.height;
      brick.anchor.x = 0.5;
      brick.anchor.y = 0.5;
      
      let brickValueText = new PIXI.Text(this.dataBrick[i].num, {fontFamily : 'Arial', fontSize: 15, padding: 35, fill : 0xffffff, align : 'center'});
      brickValueText.x = -7;
      brickValueText.y = -6;
      brick.addChild(brickValueText);
      this.addChild(brick);
    }
    this.containerLevel.addChild(this);
  }
}
