import Game from './game';
import APP from './Constant.json';

let app = new PIXI.Application({
  width: APP.APP.width,
  height: APP.APP.height,
  antialias: true,
  transparent: false,
  resolution: 1,
});
document.body.appendChild(app.view);

let game = new Game(app);

app.loader.add('data','../images/BBQ.json').add('dataFromImage', '../images/level.json').add('warning', '../images/warning.PNG').add('brick', '../images/blocks.png').load(() => game.setup());
