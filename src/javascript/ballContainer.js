import Ball from './Ball';
import Pointer from './Pointer';

export default class BallContainer extends PIXI.Container{

  constructor( x, y, dataOfBall, levelContainer, app) {
    super();
    this.xx = x;
    this.yy = y;
    this.dataOfBall = dataOfBall;
    this.levelContainer = levelContainer;
    this.check = true;
    this.app = app;
    this.pointer = new Pointer(this.xx, this.yy, this, this.check, this.levelContainer, this.app);
  }

  spawnBall() {
    let texture = PIXI.Texture.from('../images/BAL041.png');
    for(let i=0; i < this.dataOfBall.numOfBalls ; i++) {
      setTimeout(() => {
        let temp = new Ball(this.levelContainer, this.dx, this.dy,this.xx, this.yy, texture);
        this.addChild(temp);
      }
      ,50*i)
    }
  }
  changeVetorOfBall(){
    this.children.forEach((ball, index) => {
      setTimeout(() => {
        ball.dx = this.dx;
        ball.dy = this.dy;
      }, 50 * index)
    })
  }
  getPosition(x,y,dx,dy,count){
    if (count == this.dataOfBall.numOfBalls){
      setTimeout(() => {
        this.pointer.check = true;
      }, 1000);
    } 
    if (dx == 0 && dy == 0 ) {
      this.pointer.x = x;
      this.pointer.y = y;
    } 
    if (dx !== 0 && dy !== 0 ){
      this.pointer.check = false;
    }
  }
}