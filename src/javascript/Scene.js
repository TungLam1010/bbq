import Level from "./Level";
import Border from "./Border";
import * as PIXI from "pixi.js";
import DataImages from "./DataImages";
export default class Scene extends PIXI.Container {
  constructor(app) {
    super();
    this.app = app;
    this.i = 0;
    this.loadLevel();
  }
  loadLevel() {
    this.arrayLevel = [];
    let dataOfImage = new DataImages();
    let getPositionFromDataImage = this.app.renderer.extract.pixels(
      new PIXI.Sprite(Object.values(this.app.loader.resources.dataFromImage.textures)[this.i])
    );
    for (let j = 0; j < getPositionFromDataImage.length; j++) {
      if (
        getPositionFromDataImage[j] !== 0 &&
        getPositionFromDataImage[j] !== 255
      ) {
        if (Number.isInteger(j/4)) {
          this.arrayLevel.push(dataOfImage.getdataFromPixelImage(j, getPositionFromDataImage[j]));
        }
      }
    }
    
    this.arrayLevel.forEach((pixels) => {
      this.numOfBalls = (getPositionFromDataImage[dataOfImage.getPixelDataFromImage(pixels.x, pixels.y).g]);
    })
    let temp = {
      BALL: {
        numOfBalls: this.numOfBalls
      },
      BRICKS: this.arrayLevel
    }
    new Border(this.app);
    this.level = new Level(this, temp, this.app);

    this.addChild(this.level);

    this.app.stage.addChild(this);
    console.log(this.app);
  }
  update(dt) {
    this.level.update(dt);
    if (this.children[0] == undefined) {
      this.i++;
      this.loadLevel();
    }
  }
}
