import APP from './Constant.json';

export default class Score extends PIXI.Container {
  constructor(levelContainer, dataOfBricks, scenceContainer, dataOfBall, app){
    super();
    this.scenceContainer = scenceContainer;
    this.levelContainer = levelContainer;
    this.dataOfBricks = dataOfBricks;
    this.dataOfBall = dataOfBall;
    this.app = app;
    this.i = 0;
    this.onetime = false;
  }
  getScore(brick, brickY){
    if (brick !== undefined) {
      this.i++;
      this.children[5].outer.width += this.numOfHeald;
      if (this.checkScore <= this.children[5].outer.width < this.checkTwoStar) {
        this.children[1].children[2].visible = true;
        this.children[4].visible = false;
      } 
      if (this.checkTwoStar <= this.children[5].outer.width){
        this.children[1].children[1].visible = true;
        this.children[3].visible = false;
      }
    }
    this.children[0].text = (`Score: ${this.i}`);
    var sum = 0;
    for (var i = 0; i < this.dataOfBricks.length; i++) {
      sum += this.dataOfBricks[i].num;
    }
    this.numOfHeald = 220/sum;
    this.checkScore = 220/3;
    this.checkTwoStar = 220/2;
    if (this.i == sum) {
      this.children[0].text = ('Win');
      this.children[1].children[0].visible = true;
      this.children[2].visible = false;
      if (!this.onetime) {
        this.scenceContainer.removeChild(this.scenceContainer.children[0]);
        
        this.onetime = true;
      }
      
    }
    if (brickY > 1150) {
      this.i = 0; 
      this.children[5].outer.width = 1;
      this.starContainer.children[0].visible = false;
      this.starContainer.children[1].visible = false;
      this.starContainer.children[2].visible = false;
      this.children[3].visible = true;
      this.children[4].visible = true;
    }
  }
  addScore(){
    let text = new PIXI.Text(`Score: ${'0'}`,{fontFamily : 'Arial', fontSize: 40, fill : 0xffffff, align : 'center'});
    text.x = APP.APP.width/2 - text.width/2;
    this.addChild(text);
    this.starContainer = new PIXI.Container();
    this.addChild(this.starContainer);
    for (let i = 0; i < 3; i++) {
      this.blackStar = new PIXI.Sprite.from('../images/starBlack.PNG');
      this.blackStar.x = APP.APP.width/2 - APP.APP.width*0.138*i + APP.APP.width*0.138;
      this.blackStar.y = APP.APP.height*0.138;
      this.addChild(this.blackStar);
      this.starMap = new PIXI.Sprite.from('../images/star.PNG');
      this.starMap.x = APP.APP.width/2 - APP.APP.width*0.138*i + APP.APP.width*0.138;
      this.starMap.y = APP.APP.height*0.138;
      this.starContainer.addChild(this.starMap);
    }
    this.starContainer.children[0].visible = false;
    this.starContainer.children[1].visible = false;
    this.starContainer.children[2].visible = false;
    this.healdBar = new PIXI.Container();
    this.healdBar.position.set(APP.APP.width/2 - APP.APP.width*0.138, APP.APP.height*9.25925925926/100);
    this.addChild(this.healdBar);
    let innerBar = new PIXI.Graphics();
    innerBar.lineStyle(2, 0xffffff);
    innerBar.beginFill(0x000000);
    innerBar.drawRect(0, 0, 220, 8);
    innerBar.endFill();
    this.healdBar.addChild(innerBar);
    let outerBar = new PIXI.Graphics();
    outerBar.beginFill(0x4dff4d);
    outerBar.drawRect(0, 0, 1, 8);
    outerBar.endFill();
    this.healdBar.addChild(outerBar);
    this.healdBar.outer = outerBar;
    this.levelContainer.addChild(this);
  }
}