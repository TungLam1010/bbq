
export default class CollisionDetection {
  constructor(
    dx,
    dy,
    ballX,
    ballY,
    ballRadius,
    brickX,
    brickY,
    brickWidth,
    brickHeight,
    brick
  ) {
    this.b;

    this.dx = dx;
    this.dy = dy;
    this.ballX = ballX;
    this.ballY = ballY;
    this.ballRadius = ballRadius;
    this.brickX = brickX;
    this.brickY = brickY;
    this.brickWidth = brickWidth;
    this.brickHeight = brickHeight;
    this.brick = brick;
  }
  Coliision() {
    let collision = undefined;

    if (
      this.ballX + this.ballRadius + this.dx > this.brickX &&
      this.ballX + this.dx < this.brickX + this.brickWidth &&
      this.ballY + this.ballRadius > this.brickY &&
      this.ballY < this.brickY + this.brickHeight
    ) {
      collision = "left";
    }
    if (
      this.ballX + this.ballRadius > this.brickX &&
      this.ballX < this.brickX + this.brickWidth &&
      this.ballY + this.ballRadius + this.dy > this.brickY &&
      this.ballY + this.dy < this.brickY + this.brickHeight
    ) {
      collision = "top";
    }
    if (collision !== undefined) {
      this.b = this.brick;
    }
    return { collision: collision, brick: this.b };
  }
  // constructor(sprite1, sprite2){
  //   this.sprite1 = sprite1;
  //   this.sprite2 = sprite2;
  // }
  // Collision(){
  //   if (this.sprite1.x < this.sprite2.x + this.sprite2.width &&
  //     this.sprite1.x + this.sprite1.width > this.sprite2.x &&
  //     this.sprite1.y < this.sprite2.y + this.sprite2.height &&
  //     this.sprite1.y + this.sprite1.height > this.sprite2.y) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}
