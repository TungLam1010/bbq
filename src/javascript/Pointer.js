
import APP from "./Constant.json";


export default class Pointer{
  constructor(x,y, container, check, levelContainer, app){
    this.x = x;
    this.y = y;
    this.container = container;
    this.check = check;
    this.levelContainer = levelContainer;
    this.app = app;
    this.checkPles = false;
    this.line = new PIXI.Graphics();
    this.line1 = new PIXI.Graphics();
    this.dxx = 0;
    this.dyy = 0;
    this.speed = 5;
    window.addEventListener("mousemove", (e) => {
      this.mouseMoveHandler(e);
    });
    window.addEventListener('click', (e) => {
      this.clickBall(e);
    })
    this.Tutorial();
  }

  mouseMoveHandler(event) {
    if (this.check && this.checkPles) {
      let c, y1 = 0;
      this.line.clear();
      this.line1.clear();
      if (event.clientX < APP.APP.width && event.clientY < APP.APP.height - APP.APP.minOfPoinnter) {
        this.dxx =
          ((event.clientX - this.x) /
            Math.sqrt(
              Math.pow(event.clientX - this.x, 2) + Math.pow(event.clientY - this.y, 2)
            )) *
          this.speed *
          2;
        this.dyy =
          ((event.clientY - this.y) /
            Math.sqrt(
              Math.pow(event.clientX - this.x, 2) + Math.pow(event.clientY - this.y, 2)
            )) *
          this.speed *
          2;
        c = (event.clientX - this.x) / (event.clientY - this.y);
        if (c < 0) {
          y1 = (APP.APP.width - this.x) / c + this.y;

          this.line.lineStyle(4, 0xc1558d, 1);
          this.line.moveTo(this.x + 13.5, this.y);
          this.line.lineTo(APP.APP.width - 5, y1);

          this.line1.lineStyle(4, 0xc1558d, 1);
          this.line1.moveTo(APP.APP.width - 5, y1);
          this.line1.lineTo(this.x, 2 * y1 - APP.APP.height);
        } else {
          y1 = -this.x / c + this.y;

          this.line.lineStyle(4, 0xc1558d, 1);
          this.line.moveTo(this.x + 13.5, this.y);
          this.line.lineTo(5, y1);

          this.line1.lineStyle(4, 0xc1558d, 1);
          this.line1.moveTo(5, y1);
          this.line1.lineTo(this.x, 2 * y1 - APP.APP.height);
        }
        this.levelContainer.addChild(this.line);
        this.levelContainer.addChild(this.line1);
      }
    }
  }
  Tutorial(){
    this.hand = PIXI.Sprite.from('../images/hand.png');
    this.hand.x = APP.APP.width*13.8888888889/100;
    this.hand.y = APP.APP.height*74.0740740741/100;
    this.levelContainer.addChild(this.hand);

    this.tapToPlay = new PIXI.Text('TAP TO PLAY',{fontFamily : 'Arial', fontSize: 50, fill : 0xffffff, align : 'center', fontWeight: "bold"} )
    this.tapToPlay.x = APP.APP.width/2 - this.tapToPlay.width/2;
    this.tapToPlay.y = APP.APP.height/2 - APP.APP.height*18.5185185185/100;
    this.levelContainer.addChild(this.tapToPlay)
    this.tweenTap = new TWEEN.Tween(this.tapToPlay)
      .to({alpha: 0.5}, 1000)
      .yoyo(true)
      .repeat(Infinity)
      .start();
      this.tweenMove = new TWEEN.Tween(this.hand)
      .to({ x: APP.APP.width - APP.APP.width*13.8888888889/100, y: APP.APP.height*74.0740740741/100}, 1500)
      .yoyo(true)
      .repeat(Infinity)
      .onUpdate(() => {

        let c, y1 = 0;
        this.line.clear();
        this.line1.clear();
        this.dxx =
          ((this.hand.x - this.x) /
            Math.sqrt(
              Math.pow(this.hand.x - this.x, 2) + Math.pow(this.hand.y - this.y, 2)
            )) *
          this.speed *
          2;
        this.dyy =
          ((this.hand.y - this.y) /
            Math.sqrt(
              Math.pow(this.hand.x - this.x, 2) + Math.pow(this.hand.y - this.y, 2)
            )) *
          this.speed *
          2;
        c = (this.hand.x - this.x) / (this.hand.y - this.y);
        if (c < 0) {
          y1 = (APP.APP.width - this.x) / c + this.y;

          this.line.lineStyle(4, 0xc1558d, 1);
          this.line.moveTo(this.x + 13.5, this.y);
          this.line.lineTo(APP.APP.width - 5, y1);

          this.line1.lineStyle(4, 0xc1558d, 1);
          this.line1.moveTo(APP.APP.width - 5, y1);
          this.line1.lineTo(this.x, 2 * y1 - APP.APP.height);
        } else {
          y1 = -this.x / c + this.y;

          this.line.lineStyle(4, 0xc1558d, 1);
          this.line.moveTo(this.x + 13.5, this.y);
          this.line.lineTo(5, y1);

          this.line1.lineStyle(4, 0xc1558d, 1);
          this.line1.moveTo(5, y1);
          this.line1.lineTo(this.x, 2 * y1 - APP.APP.height);
        }
        this.levelContainer.addChild(this.line);
        this.levelContainer.addChild(this.line1);
      }).start()
    
    
  }
  clickBall(event) {
    if (this.check) {
      if (event.clientX < APP.APP.width && event.clientY < APP.APP.height) {
        TWEEN.remove(this.tweenMove, this.tweenTap);
        this.levelContainer.removeChild(this.hand);
        this.levelContainer.removeChild(this.tapToPlay);
        this.checkPles = true;
        this.container.dx = this.dxx;
        this.container.dy = this.dyy;
        this.line.clear();
        this.line1.clear();
        this.container.getPosition();
        if (this.container.children[0] == undefined) {
          this.container.spawnBall();
        } else {
          this.container.changeVetorOfBall();
        }
      }
    }
  }
}