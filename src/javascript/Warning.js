import APP from './Constant.json';
export default class Warning extends PIXI.Container {
  constructor(containerLevel, app){
    super();
    this.containerLevel = containerLevel;
    this.app = app;
    this.addWarning();
  }
  addWarning(){
    this.containerLevel.addChild(this);
    for (let i = 0; i < (APP.APP.width / 50) - 1; i++) {
      let spriteWarning = new PIXI.Sprite(this.app.loader.resources.warning.texture);
      let scaleFactor = 50/spriteWarning.width;
      spriteWarning.scale = new PIXI.ObservablePoint(null, spriteWarning, scaleFactor, scaleFactor);
      spriteWarning.y = APP.APP.height - 50-5;
      spriteWarning.x = 10 + 50 * i;
      this.addChild(spriteWarning);
    }
  }
}