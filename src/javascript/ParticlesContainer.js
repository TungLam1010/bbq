
import emitter from './emitter.json';
import * as particles from 'pixi-particles';
export default class ParticlesContainer {
  constructor(bg, numb) {
    this.bg = bg;
    this.asset = new PIXI.Texture.from("../images/glow.png");
    this.arr = [];
    this.emitterContainer = new PIXI.Container();
    for (let i = 0; i < numb; i++) {
      var temp = {};
      this.bg.addChild(this.emitterContainer);
      temp.data = new particles.Emitter(
        this.emitterContainer,
        this.asset,
        emitter
      );
      temp.isUsed = false;
      this.arr.push(temp);
    }

  }
  update(dt) {
    this.arr.forEach((child) => {
      if (child.isUsed) {
        child.data.update(dt * 0.02);
        child.data.emit = false;
        if (!child.timeOut) {
          child.timeOut = true;
          setTimeout(() => {
            child.data.emit = true;
            child.isUsed = false;
            child.timeOut = false;
          }, 3000);
        }
      }
    });
  }
  

  getChild() {
    var res;
    for (var i = 0; i < this.arr.length; i++) {
      if (!this.arr[i].isUsed) {
        this.arr[i].isUsed = true;
        res = this.arr[i];
        break;
      }
    }
    return res;
  }
}