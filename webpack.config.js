const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: "./src/javascript/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "",
    filename: "bundle.js",
  },
  plugins: [
    // new CleanWebpackPlugin(),
    new HtmlWebpackPlugin(),
    new webpack.ProvidePlugin ({
      PIXI: 'pixi.js',
      TWEEN: '@tweenjs/tween.js',
      particles: 'pixi-particles',
      Howl : 'howler'
    }),
    new CopyPlugin({
        patterns: [
            { from: 'src/images', to: 'images' },
        ]
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.js$/,
      },
      {
        test: /\.(sa|sc|c)ss$/,
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
               {
                 loader: "file-loader",
                 options: {
                   outputPath: './assets/images'
                 }
               }
             ]
      }
    ],
  },
  mode: "development",
};
